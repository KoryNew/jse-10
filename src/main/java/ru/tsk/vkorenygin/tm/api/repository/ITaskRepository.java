package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task Task);

    void remove(Task Task);

    List<Task> findAll();

    void clear();

}
