package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
