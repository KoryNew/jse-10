package ru.tsk.vkorenygin.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();
}
