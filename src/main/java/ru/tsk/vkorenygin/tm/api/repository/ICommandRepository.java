package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
