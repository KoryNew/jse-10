package ru.tsk.vkorenygin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();
}
